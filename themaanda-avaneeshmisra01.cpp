// -------------
// TestDeque.cpp
// -------------

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test
{
    using deque_type = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
        deque<int>,
        my_deque<int>,
        deque<int, allocator<int>>,
        my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test0)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, test1)
{
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test2)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(3, 6);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_TRUE(equal(b, e, begin({6, 6, 6})));
}

// ------------------
// deque(size_type s)
// ------------------

TYPED_TEST(DequeFixture, test4)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(12);
    ASSERT_EQ(x.size(), 12u);
}

TYPED_TEST(DequeFixture, test5)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(0);
    ASSERT_EQ(x.size(), 0u);
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test6)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(4, 5);
    ASSERT_EQ(x.size(), 4u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({5, 5, 5, 5})));
}

TYPED_TEST(DequeFixture, test7)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1, 5);
    ASSERT_EQ(x.size(), 1u);
    ASSERT_TRUE(!x.empty());
}

TYPED_TEST(DequeFixture, test8)
{
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    deque_type x(1, 5, allocator_type());
    ASSERT_EQ(x.size(), 1u);
    ASSERT_TRUE(equal(begin(x), end(x), begin({5})));
}

TYPED_TEST(DequeFixture, test9)
{
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    deque_type x(0, 5, allocator_type());
    ASSERT_EQ(x.size(), 0u);
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test10)
{
    using deque_type = typename TestFixture::deque_type;
    using allocator_type = typename TestFixture::allocator_type;
    const initializer_list<int> y = {1, 2, 3, 4, 5};
    deque_type x(y, allocator_type());
    ASSERT_EQ(x.size(), 5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4, 5})));
}

TYPED_TEST(DequeFixture, test11)
{
    using deque_type = typename TestFixture::deque_type;
    const initializer_list<int> y = {1, 2};
    const deque_type x = y;
    ASSERT_NE(&x[1], &(y.begin()[1]));
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2})));
}

TYPED_TEST(DequeFixture, test12)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type y(5, 2);
    ASSERT_EQ(y.size(), 5);
    deque_type x(y);
    ASSERT_EQ(x.size(), 5);
    ASSERT_NE(&x[0], &y[0]);
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test13)
{
    using deque_type = typename TestFixture::deque_type;
    const deque_type y(1, 3);
    ASSERT_EQ(y.size(), 1);
}

TYPED_TEST(DequeFixture, test14)
{
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const initializer_list<int> y = {2, 3, 4};
    const deque_type x(y);
    const_iterator b = begin(x);
    ASSERT_EQ(*b, 2);
    ASSERT_EQ(*(++b), 3);
}

TYPED_TEST(DequeFixture, test15)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(0);
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test16)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(3, 5);
    ASSERT_TRUE(!x.empty());
}

TYPED_TEST(DequeFixture, test17)
{
    using deque_type = typename TestFixture::deque_type;
    const initializer_list<int> y = {1, 2, 3};
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(y);
    const_iterator e = end(x);
    ASSERT_EQ(*(--e), 3);
}

TYPED_TEST(DequeFixture, test18)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.erase(begin(x));
    ASSERT_EQ(*(begin(x)), 2);
}

TYPED_TEST(DequeFixture, test19)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {10, 5, 0};
    deque_type x(y);
    x.erase(begin(x));
    ASSERT_EQ(*(begin(x)), 5);
}

TYPED_TEST(DequeFixture, test20)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.insert(++begin(x), 10);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 10, 2, 3})));
}

TYPED_TEST(DequeFixture, test21)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.insert(end(x), 4);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4})));
}

TYPED_TEST(DequeFixture, test22)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.insert(begin(x), 1);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4})));
}

TYPED_TEST(DequeFixture, test23)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.pop_back();
    ASSERT_TRUE(x.size() == 2);
}

TYPED_TEST(DequeFixture, test24)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.pop_back();
    x.pop_back();
    ASSERT_EQ(x.size(), 1);
}

TYPED_TEST(DequeFixture, test25)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.pop_front();
    ASSERT_TRUE(equal(begin(x), end(x), begin({2, 3})));
}

TYPED_TEST(DequeFixture, test26)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.pop_front();
    x.pop_front();
    ASSERT_EQ(x.size(), 1);
}

TYPED_TEST(DequeFixture, test27)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4};
    deque_type x(y);
    x.push_back(5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4, 5})));
}

TYPED_TEST(DequeFixture, test28)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.push_back(4);
    x.push_back(5);
    ASSERT_TRUE(!x.empty());
    ASSERT_EQ(x.size(), 5);
}

TYPED_TEST(DequeFixture, test29)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4};
    deque_type x(y);
    x.push_front(0);
    ASSERT_TRUE(equal(begin(x), end(x), begin({0, 1, 2, 3, 4})));
}

TYPED_TEST(DequeFixture, test30)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.push_front(1);
    ASSERT_EQ(x.size(), 4);
}

TYPED_TEST(DequeFixture, test31)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.resize(1, 4);
    x.resize(1, 5);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4, 5})));
}

TYPED_TEST(DequeFixture, test32)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x = y;
    ASSERT_TRUE(equal(begin(x), end(x), begin(y)));
}

TYPED_TEST(DequeFixture, test33)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3, 4, 5};
    deque_type x = y;
    x.resize(3, 4);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 4})));
}

TYPED_TEST(DequeFixture, test34)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1, 2, 3};
    deque_type x(y);
    x.resize(4);
    x.push_back(-1);
    ASSERT_TRUE(equal(begin(x), end(x), begin({1, 2, 3, 0, -1})));
}

TYPED_TEST(DequeFixture, test35)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.resize(0);
    ASSERT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test36)
{
    using deque_type = typename TestFixture::deque_type;
    deque_type x(0, 1);
    ASSERT_TRUE(x.size() == 0);
}

TYPED_TEST(DequeFixture, test37)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {2, 3, 4};
    deque_type x(y);
    x.resize(10);
    ASSERT_TRUE(x.size() == 10);
}

TYPED_TEST(DequeFixture, test38)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {5, 6, 7};
    deque_type x(y);
    ASSERT_TRUE(x.size() == 3);
}

TYPED_TEST(DequeFixture, test39)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1};
    deque_type x(y);
    ASSERT_EQ(x.front(), 1);
}

TYPED_TEST(DequeFixture, test40)
{
    using deque_type = typename TestFixture::deque_type;
    initializer_list<int> y = {1};
    deque_type x(y);
    x.push_front(8);
    ASSERT_EQ(x.front(), 8);
    x.pop_front();
    ASSERT_EQ(x.front(), 1);
}
