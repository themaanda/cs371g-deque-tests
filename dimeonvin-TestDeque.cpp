// -------------
// TestDeque.cpp
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

template <typename T>
struct DequeFixture2 : Test {
    using deque_type2    = T;
    using allocator_type = typename deque_type2::allocator_type;
    using iterator       = typename deque_type2::iterator;
    using const_iterator = typename deque_type2::const_iterator;
};

using deque_types2 =
    Types<
    deque<deque<int>>,
    my_deque<deque<int>>,
    deque<deque<int>, allocator<deque<int>>>,
    my_deque<deque<int>, allocator<deque<int>>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
TYPED_TEST_CASE(DequeFixture2, deque_types2,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
TYPED_TEST_CASE(DequeFixture2, deque_types2);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    EXPECT_TRUE(x.size() == 0u);
}

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test2) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    const deque_type y;
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test5) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(3);
    y.push_back(3);
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test6) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(3);
    y.push_back(6);
    EXPECT_TRUE(x != y);
}

TYPED_TEST(DequeFixture, test7) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(3);
    x.push_back(6);
    y.push_back(3);
    EXPECT_TRUE(x != y);
}

TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(3);
    y.push_back(6);
    EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    x.push_back(10000);
    x.push_back(100);
    y.push_back(10000);
    y.push_back(100);
    y.push_back(3);
    EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    for (int i = 0; i < 100; ++i) {
        x.push_back(i);
        y.push_back(i);
    }
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    for (int i = 0; i < 100; ++i) {
        x.push_back(i);
        y.push_back(i + 100);
    }
    EXPECT_TRUE(x != y);
}

TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    for (int i = 0; i < 100; ++i) {
        x.push_front(i);
        y.push_front(i);
    }
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    deque_type y;
    for (int i = 0; i < 100; ++i) {
        x.push_front(i);
        y.push_front(i + 100);
    }
    EXPECT_TRUE(x != y);
}

TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 100; ++i) {
        x.push_back(i);
    }
    for (int i = 0; i < 100; ++i) {
        EXPECT_EQ(x[i], i);
    }
}

TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;

    for (int i = 0; i < 100; ++i) {
        x.push_back(i);
        x.pop_back();
    }
    EXPECT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;

    for (int i = 0; i < 100; ++i) {
        x.push_front(i);
        x.pop_front();
    }
    EXPECT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 10; ++i) {
        x.push_front(i);
    }
    x.insert(x.begin(), 5);
    EXPECT_TRUE(x.begin() != x.end());
}

TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100);
    EXPECT_TRUE(x.size() == 100);
}

TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 3);
    EXPECT_TRUE(x[50] == 3);
}

TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1, 0);
    x.insert(x.end(), 1);
    for (std::deque<int>::size_type i = 0; i < x.size(); ++i) {
        ASSERT_EQ(x[i], i);
    }
}

TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1, 1);
    for (int i = 0; i < 100; ++i) {
        x.push_back(3);
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 1);
}

TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(1, 3);
    x.erase(x.begin());
    ASSERT_EQ(x.size(), 0);
}

// Second DequeFixture

TYPED_TEST(DequeFixture2, test23) {
    using deque_type2 = typename TestFixture::deque_type2;
    const deque_type2 x(100);
    EXPECT_EQ(x.size(), 100);
}

TYPED_TEST(DequeFixture2, test24) {
    using deque_type2 = typename TestFixture::deque_type2;
    deque_type2 x;
    deque_type2 y;
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture2, test25) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10);
    deque_type2 x;
    deque_type2 y;
    x.push_back(a);
    y.push_back(a);
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture2, test26) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x;
    deque_type2 y;
    x.push_front(a);
    y.push_front(a);
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture2, test27) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x;
    for (int i = 0; i < 100; ++i) {
        x.push_back(a);
        x.pop_back();
    }
    EXPECT_TRUE(x.size() == 0);
}

TYPED_TEST(DequeFixture2, test28) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x;
    for (int i = 0; i < 100; ++i) {
        x.push_back(a);
    }
    EXPECT_TRUE(x.at(50) == a);
}

TYPED_TEST(DequeFixture2, test29) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x(1, a);
    x.insert(x.begin(), a);
    EXPECT_TRUE(x.size() == 2);
}

TYPED_TEST(DequeFixture2, test30) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x(1, a);
    x.erase(x.begin());
    EXPECT_TRUE(x.size() == 0);
}

TYPED_TEST(DequeFixture2, test31) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x;
    x.push_front(a);
    x.push_back(a);
    x.erase(x.end() - 1);
    EXPECT_TRUE(x.at(0) == a);
}


TYPED_TEST(DequeFixture2, test32) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type b(10, 3);
    deque_type2 x;
    deque_type2 y;
    x.push_front(a);
    y.push_front(b);
    EXPECT_TRUE(x == y);
}

TYPED_TEST(DequeFixture2, test33) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type b(10, 100);
    deque_type2 x;
    deque_type2 y;
    x.push_front(a);
    y.push_front(b);
    EXPECT_TRUE(x < y);
}

TYPED_TEST(DequeFixture2, test34) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x;
    x.resize(10000);
    EXPECT_TRUE(x.size() == 10000);
}

TYPED_TEST(DequeFixture2, test35) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x;
    x.push_back(a);
    x.resize(0);
    EXPECT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture2, test36) {
    using deque_type2 = typename TestFixture::deque_type2;
    using deque_type = typename deque_type2::value_type;
    deque_type a(10, 3);
    deque_type2 x;
    x.push_back(a);
    EXPECT_TRUE(!x.empty());
}

TYPED_TEST(DequeFixture2, test37) {
    using deque_type2 = typename TestFixture::deque_type2;
    deque_type2 x(1);
    x.pop_front();
    EXPECT_TRUE(x.empty());
}

TYPED_TEST(DequeFixture, test38) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 5);
    EXPECT_TRUE(x.at(50) == 5);
}

TYPED_TEST(DequeFixture, test39) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 5);
    x.push_front(10000);
    EXPECT_TRUE(x.at(0) == 10000);
}

TYPED_TEST(DequeFixture, test40) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 5);
    x.insert(x.begin() + 50, 10000);
    EXPECT_TRUE(x.at(50) == 10000);
}